# Cache

# Periodic Self-Rehydrating Cache

Spec v1 (2022-03-30)

This cache is able to register 0-arity functions (each under a new key) that will recompute periodically and store their results in the cache for fast-access instead of being called every time the values are needed.

## Requirements and context for the exercise

This caching mechanism is useful when we are working with data that doesn't change often and its benefits become clear if computing the data is expensive in the first place.

As an example, let's consider an application that makes multiple queries to an external service returning weather data categorized by cities. Because of API rate-limiting, the queries could take multiple minutes or more to execute but the weather can be fast changing. In order to have fresh data in the cache at all times, we can register a function like `:weather_data` with a `ttl` ("time to live") of 1 hour and a `refresh_interval` of 10 minutes. Similarly to a [cron job](https://en.wikipedia.org/wiki/Cron), the function is executed at a given interval of time and the cache holds the most recently computed value and can provide it as needed.


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cache` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cache, "~> 0.1.0"}
  ]
end
```
## Usage

### Register a function for a key :

```elixir
Cache.register_function(
        fun,
        key,
        ttl,
        refresh_interval
      )

```

Exemple :

```elixir
 Cache.register_function(
        fn ->
          value = long_processing_work()
          {:ok, value}
        end,
        "key",
        50000,
        1000
      )
 ```     


 ### Get the value for a key :

 

 ```elixir
  result = Cache.get("key", timeout)
 ```

Where result is one of `{:ok, value}`, `{:error, reason}`

 Exemple :
 ```elixir
  {ok, value} = Cache.get("test_key", 3000)
  ```

  ## Another approach

  [MQTT protocol](https://mqtt.org/) would also be ideally suited to address this issue, using [retained messages](https://www.hivemq.com/blog/mqtt-essentials-part-8-retained-messages/).

  MQTT offers low footprint pub/sub service where a "topic" can represent a key of the cache.

  Clients interested in this key would subscribe to this topic ( some Access Control can occur).

  A worker, would run the hydrating function and publish the value to the topic matching the registered key. This publish would set the "retained" flag to true.

  [Retained messages](https://www.hivemq.com/blog/mqtt-essentials-part-8-retained-messages/), to make it simple, permits to create topics containing only the last published message/value. So any subscriber to the topic will get the last published value when it connects to topic, and keep receiving updates as long as it is connected.

  Upon the choosen MQTT broker this could permit to support storing in database each value in an easy an convenient way.
  
  As said, it would bring access control. 
  
  It would also, upon choosen broker brings scalability and easy clusturing.
  
  It would also bring connectivity to other transport protocols ( kafka, rabbit, Rest etc).
  

  